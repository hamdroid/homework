/**
 * Created by hamdroid on 22/02/2017.
 */

export default function counterFactory() {

    return {
        "Counter": Counter
    };

    function Counter() {
        let i = 0;

        this.inc = inc;
        this.dec = dec;
        this.get = get;
        this.reset = reset;

        function inc(max) {
            i = i + 1;
            if (i === max) {
                reset();
            }
        }

        function dec() {
            i = i - 1;
            if (i < 0) {
                reset();
            }
        }

        function get() {
            return i;
        }

        function reset() {
            i = 0;
        }
    }
}
