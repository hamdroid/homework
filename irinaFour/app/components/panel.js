/**
 * Created by hamdroid on 21/02/2017.
 */

export default function panelFactory(m) {

    return {
        "Panel": Panel
    };

    function Panel(counter, styles) {
        this.getView = getView;

        function getView() {
            return {
                "view": view
            };
        }

        function view() {
            return m("div",
                css(`panel ${styles[counter.get()]}`),
                [
                    m("div",
                        css("panel panel-heading"),
                        headerText()
                    ),
                    m("div",
                        css("panel-body"),
                        [
                            m("button",
                                click(buttonClick),
                                buttonText()
                            )
                        ]
                    )
                ]
            );

            function click(clickHandler){
                return {
                    "onclick": clickHandler
                };
            }

            function css(classString) {
                return {
                    "class": classString
                };
            }

            function headerText() {
                return `Current Style ${styles[counter.get()]}`;
            }

            function buttonClick() {
                counter.inc(styles.length);
            }

            function buttonText() {
                return `current index ${counter.get()}`
            }
        }
    }
}
