require('../css/style.css');
require('../node_modules/bootstrap/dist/css/bootstrap.css');

import m from 'mithril';
import panel from './components/panel';
import counter from './components/counter';

(function () {

    let styles = [
        "panel-default",
        "panel-primary",
    ];

    let extendedStyles =
        [].concat(
            styles,
            [
                "panel-danger",
                "panel-warning"
            ]
        );

    let panelFactory = panel(m);
    let counterFactory = counter();

    let myCounter = new counterFactory.Counter();
    let freePanel = new panelFactory.Panel(
        new counterFactory.Counter(),
        styles
    );

    m.mount(
        document.getElementById("myRoot"),
        {
            "view": function () {

                let panels = [],
                    i,
                    count = 5;

                panels.push(
                    m(freePanel.getView())
                );

                for (i = 0; i < count; i = i + 1) {
                    panels.push(
                        m(new panelFactory.Panel(myCounter, extendedStyles).getView())
                    );
                }
                return panels;
            }
        }
    );
}());
