/**
 * Created by adhach29 on 24.02.2017.
 */

import counter from '../../app/components/counter';

describe('My Counter', () => {

    let counterFactory = counter();
    let myCounter;

    beforeEach(() => {
        myCounter = new counterFactory.Counter();
    });

    it('should be 1 if inc is called once', () => {
        myCounter.inc();
        expect(myCounter.get()).toBe(1);
    });

    it('should be 10 if inc is called 10 times', () => {
        let i = 0;
        let limit = 10;

        for (i; i < limit; i = i + 1) {
            myCounter.inc();
        }

        expect(myCounter.get()).toBe(limit);
    });

    it('should be one smaller if dec is called', () => {

        myCounter.inc();
        myCounter.dec();

        expect(myCounter.get()).toBe(0);
    });

    it('cannot be decremented below 0', () =>{
        myCounter.dec();
        myCounter.dec();
        myCounter.dec();
        myCounter.dec();

        expect(myCounter.get()).toBe(0);
    });

    it('should be 0 if reset is called', () => {
        let i = 0;
        let limit = 10;

        for (i; i < limit; i = i + 1) {
            myCounter.inc();
        }

        myCounter.reset();

        expect(myCounter.get()).toBe(0);
    });
});