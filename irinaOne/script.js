/**
 * Created by hamdroid on 10/02/2017.
 */

(function (pId) {

    var element = document.getElementById(pId);
    var colors = [
            setColorOnElement(element, "black"),
            setColorOnElement(element, "red"),
            setColorOnElement(element, "yellow")
        ],
        i = 0,
        interval = 1000;

    setInterval(function () {

        colors[i]();
        i = i + 1;
        if (i === colors.length) {
            i = 0;
        }
    }, interval);
}("myText"));

function setColorOnElement(element, color) {
    return function () {
        element.className = color;
    };
}