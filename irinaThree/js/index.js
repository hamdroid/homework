/**
 * Created by hamdroid on 19/02/2017.
 */

(function ($, ml) {

    var myCanvas = $('#myCanvas');
    var log = new ml.Logger();
    var myPanel = new ml.Panel(
        myCanvas
    );
    var anotherPanel = new ml.Panel(
        myCanvas
    );

    myPanel.setState("bad");
    myPanel.setTitle("Hey guy", log.log);
    myPanel.setContent("here I am", log.log);

    anotherPanel.setTitle("Hey guy", log.log);
    anotherPanel.setContent("here I am", log.log);

}(jQuery, myLib));