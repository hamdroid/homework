/**
 * Created by hamdroid on 19/02/2017.
 */

var myLib = myLib || {};

(function ($) {

    function Panel(parent) {

        var states = {
            "ok": "panel-primary",
            "bad": "panel-danger"
        };

        var currentState = "ok";

        var panel =
            $("<div></div>")
                .addClass("panel panel-primary");

        var heading =
            $("<div></div>")
                .addClass("panel panel-heading");

        var body =
            $("<div></div>")
                .addClass("panel-body");

        panel
            .append(heading)
            .append(body);

        parent
            .append(panel);


        this.setState = setState;
        function setState(state) {
            if (!states.hasOwnProperty(state)) {
                return;
            }
            panel.removeClass(states[currentState]);
            panel.addClass(states[state]);
            currentState = state;
        }

        this.setTitle = setter(heading);
        this.setContent = setter(body);

        function setter(where) {
            return function (msg, cb) {
                where.html("<span>" + msg + "</span>");
                cb(msg + " has been set");
            }
        }
    }

    myLib.Panel = Panel;

}(jQuery));