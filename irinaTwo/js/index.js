(function () {
    populateMenu(
        document.getElementById("firstList"),
        [
            "Something",
            "Something",
            "Dark Side"
        ]
    );

    populateMenu(
        document.getElementById("secondList"),
        [
            "In",
            "a",
            "galaxy",
            "far",
            "far",
            "away"
        ]
    );

    function populateMenu(menu, menuItems) {
        _.each(menuItems, createMenuItem);

        function createMenuItem(menuItem) {
            var menuElement = document.createElement("li");
            menuElement.innerHTML =
                [
                    "<a href='#' onclick=\"alert('",
                    menuItem,
                    "')\">",
                    menuItem,
                    "</a>"
                ].join("");
            menu.appendChild(menuElement);
        }
    }
}());